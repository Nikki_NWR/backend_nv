package pro.devleaders.datatrainer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataTrainerBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataTrainerBackendApplication.class, args);
	}

}
