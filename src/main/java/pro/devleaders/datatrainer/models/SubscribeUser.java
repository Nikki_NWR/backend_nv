package pro.devleaders.datatrainer.models;


import javax.persistence.*;

@Entity
@Table(name = "subscribe_users")
public class SubscribeUser {

    @Id
    @Column
    private long targetUserId;

    @ManyToOne
    @JoinColumn(name = "subscribeUserId", nullable = false)
    private User user;

    public long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "SubscribeUser{" +
                "targetUserId=" + targetUserId +
                ", user=" + user +
                '}';
    }
}
