package pro.devleaders.datatrainer.models;

import lombok.*;

import javax.persistence.Table;
import java.util.Set;

@Getter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "user")
public class UserView extends AuditModel{
	private Long id;
	private String username;
	private String email;
	private Profile profile;
	private Set<User> userFriends;
	private Set<User> subscribeUsers;
	private Set<User> blockUsers;
	private Set<User> trainer;
	private Set<User> sportsman;

	public UserView(User user) {
//		final Profile profile = user.getProfile();

		this.id = user.getId();
		this.username = user.getUsername();
		this.email = user.getEmail();
		this.profile = user.getProfile();
		this.userFriends = user.getUserFriends();
		this.subscribeUsers = user.getSubscribeUsers();
		this.blockUsers = user.getBlockUsers();
		this.trainer = user.getTrainer();
		this.sportsman = user.getSportsman();
	}
}
