package pro.devleaders.datatrainer.models;

public enum ESport {
    SPORT_RUN,
    SPORT_SWIM,
    SPORT_CYCLING
}
