package pro.devleaders.datatrainer.models;


import lombok.Getter;
import lombok.ToString;
import java.util.Date;

@Getter
@ToString
public class HealthView extends AuditModel {

    private Long id;
    private String bad;
    private String chair;
    private String urine;
    private float weight;
    private String sleep;
    private String sam;
    private String pes;
    private String muscles;
    private String mchk;
    private String sport;
    private String training;
    private int rpe;
    private String la;
    private String breath;
    private String sweat;
    private float degrees;
    private Date date;
    private Long creatorId;

    public HealthView( Health health) {

        final User creator = health.getCreator();

        this.bad = health.getBad();
        this.chair = health.getChair();
        this.urine = health.getUrine();
        this.weight = health.getWeight();
        this.sleep = health.getSleep();
        this.sam = health.getSam();
        this.pes = health.getPes();
        this.muscles = health.getMuscles();
        this.mchk = health.getMchk();
        this.sport = health.getSport();
        this.training = health.getTraining();
        this.rpe = health.getRpe();
        this.la =  health.getLa();
        this.breath = health.getBreath();
        this.sweat =  health.getSweat();
        this.degrees = health.getDegrees();
        this.date = health.getDate();
        this.creatorId = creator.getId();
    };


}
