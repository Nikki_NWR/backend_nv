

package pro.devleaders.datatrainer.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(	name = "testpost")
public class TestPost extends AuditModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Size(max = 4000)
    private String text;

    @Size(max = 140)
    private String prop_1;

    @Size(max = 140)
    private String prop_2;

    @Size(max = 140)
    private String prop_3;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ProfileId", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JsonProperty("ProfileId")
    @ApiModelProperty(notes = "Userid of the person who posted the post")
    private Profile profile;

    public TestPost(){ };

    public TestPost(String text,String prop_1,String prop_2,String prop_3) {
        this.text =text;
        this.prop_1 = prop_1;
        this.prop_2 = prop_2;
        this.prop_3 = prop_3;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getProp_1() { return prop_1; }

    public void setProp_1(String prop_2) { this.prop_1 = prop_1; }

    public String getProp_2() { return prop_2; }

    public void setProp_2(String prop_2) { this.prop_2 = prop_2; }

    public String getProp_3() { return prop_3; }

    public void setProp_3(String prop_3) { this.prop_3 = prop_3; }

    public void setProfile(Profile profile){this.profile = profile;}

    @Override
    public String toString() {
        return "TestPost{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", prop_1='" + prop_1 + '\'' +
                ", prop_2='" + prop_2 + '\'' +
                ", prop_3='" + prop_3 + '\'' +
                ", profile=" + profile +
                '}';
    }
}