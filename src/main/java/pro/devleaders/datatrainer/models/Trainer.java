package pro.devleaders.datatrainer.models;

import javax.persistence.*;

@Entity
@Table(name = "users_trainer")
public class Trainer{
    @Id
    @Column
    private long trainerId;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    public long getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(long trainerId) {
        this.trainerId = trainerId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "trainerId=" + trainerId +
                ", user=" + user +
                '}';
    }
}
