package pro.devleaders.datatrainer.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(	name = "users",
		uniqueConstraints = { 
			@UniqueConstraint(columnNames = "username"),
			@UniqueConstraint(columnNames = "email")
		})
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = {"profile"})
public class User extends AuditModel{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String username;

	@NotBlank
	@Size(max = 50)
	@Email
	private String email;

	@NotBlank
	@Size(max = 120)
	@JsonIgnore
	private String password;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "user_roles", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();


	@JsonBackReference
	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "profile", nullable = false)
	@ApiModelProperty(notes = "Property linking the userprofile with the user")
	private Profile profile;

	@ManyToMany
	@JsonIgnore
	@JoinTable(name = "users_friends", joinColumns = @JoinColumn(name = "userId") , inverseJoinColumns = @JoinColumn(name = "friendId") )
	private Set<User> userFriends;

	@ManyToMany
	@JsonIgnore
	@JoinTable(name = "subscribe_users", joinColumns = @JoinColumn(name = "subscribeUserId") , inverseJoinColumns = @JoinColumn(name = "targetUserId") )
	private Set<User> subscribeUsers;

	@ManyToMany
	@JsonIgnore
	@JoinTable(name = "block_users", joinColumns = @JoinColumn(name = "blockUserId") , inverseJoinColumns = @JoinColumn(name = "targetUserId") )
	private Set<User> blockUsers;

	@ManyToMany
	@JsonIgnore
	@JoinTable(name = "users_trainer", joinColumns = @JoinColumn(name = "userId") , inverseJoinColumns = @JoinColumn(name = "trainerId") )
	private Set<User> trainer;

	@ManyToMany
	@JsonIgnore
	@JoinTable(name = "users_sportsman", joinColumns = @JoinColumn(name = "userId") , inverseJoinColumns = @JoinColumn(name = "sportsmanId") )
	private Set<User> sportsman;

	public User() {
	}

	public User(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<User> getUserFriends() {
		return userFriends;
	}

	public void setUserFriends(Set<User> userFriends) {
		this.userFriends = userFriends;
	}

	public Set<User> getSubscribeUsers() {
		return subscribeUsers;
	}

	public void setSubscribeUsers(Set<User> subscribeUsers) {
		this.subscribeUsers = subscribeUsers;
	}

	public Set<User> getBlockUsers() {
		return blockUsers;
	}

	public void setBlockUsers(Set<User> blockUsers) {
		this.blockUsers = blockUsers;
	}

	public Set<User> getTrainer() {
		return trainer;
	}

	public void setTrainer(Set<User> trainer) {
		this.trainer = trainer;
	}

	public Set<User> getSportsman() {
		return sportsman;
	}

	public void setSportsman(Set<User> sportsman) {
		this.sportsman = sportsman;
	}


	public void addUserFriends(User user) {
		if (CollectionUtils.isEmpty(this.userFriends)) {
			this.userFriends = new HashSet<>();
		}
		this.userFriends.add(user);
	}

	public void addSubscribeUsers(User user) {
		if (CollectionUtils.isEmpty(this.subscribeUsers)) {
			this.subscribeUsers = new HashSet<>();
		}
		this.subscribeUsers.add(user);
	}

	public void addBlockUsers(User user) {
		if (CollectionUtils.isEmpty(this.blockUsers)) {
			this.blockUsers = new HashSet<>();
		}
		this.blockUsers.add(user);
	}

	public void addUserTrainer(User user) {
		if (CollectionUtils.isEmpty(this.trainer)) {
			this.trainer = new HashSet<>();
		}
		this.trainer.add(user);
	}

	public void addUserSportsman(User user) {
		if (CollectionUtils.isEmpty(this.sportsman)) {
			this.sportsman = new HashSet<>();
		}
		this.sportsman.add(user);
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", username='" + username + '\'' +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", roles=" + roles +
				", profile=" + profile +
				", userFriends=" + userFriends +
				", subscribeUsers=" + subscribeUsers +
				", blockUsers=" + blockUsers +
				", trainer=" + trainer +
				", sportsman=" + sportsman +
				'}';
	}
}
