package pro.devleaders.datatrainer.models;

import java.util.List;

public class SportsmanRequestEntity {

    private List<String> sportsman;

    public List<String> getSportsman() {
        return sportsman;
    }

    public void setSportsman(List<String> sportsman) {
        this.sportsman = sportsman;
    }
}
