package pro.devleaders.datatrainer.models;

import javax.persistence.*;

@Entity
@Table(name = "block_users")
public class BlockUser {

    @Id
    @Column
    private long targetUserId;

    @ManyToOne
    @JoinColumn(name = "blockUserId", nullable = false)
    private User user;

    public long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
