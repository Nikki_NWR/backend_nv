package pro.devleaders.datatrainer.models;

import lombok.*;
import javax.persistence.*;
import java.util.Date;


@Data
@Table(name = "health")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class HealthPost extends AuditModel{
    private String bad;
    private String chair;
    private String urine;
    private float weight;
    private String sleep;
    private String sam;
    private String pes;
    private String muscles;
    private String mchk;
    private String sport;
    private String trainig;
    private int rpe;
    private String la;
    private String breath;
    private String sweat;
    private float degrees;
    private Date date;
}