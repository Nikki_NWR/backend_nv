package pro.devleaders.datatrainer.models;

import javax.persistence.*;

@Entity
@Table(name = "sports")
public class Sport {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private ESport name;

	public Sport() {

	}

	public Sport(ESport name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ESport getName() {
		return name;
	}

	public void setName(ESport name) {
		this.name = name;
	}
}