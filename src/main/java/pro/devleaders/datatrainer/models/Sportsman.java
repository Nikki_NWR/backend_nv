package pro.devleaders.datatrainer.models;

import javax.persistence.*;

@Entity
@Table(name = "users_sportsman")
public class Sportsman{

    @Id
    @Column
    private long sportsmanId;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    public long getSportsmanId() {
        return sportsmanId;
    }

    public void setSportsmanId(long sportsmanId) {
        this.sportsmanId = sportsmanId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Sportsman{" +
                "sportsmanId=" + sportsmanId +
                ", user=" + user +
                '}';
    }
}
