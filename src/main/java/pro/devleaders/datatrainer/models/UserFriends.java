package pro.devleaders.datatrainer.models;

import javax.persistence.*;

@Entity
@Table(name = "users_friends")
public class UserFriends {
    @Id
    @Column
    private long friendId;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    public long getFriendId() {
        return friendId;
    }

    public void setFriendId(long friendId) {
        this.friendId = friendId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserFriends{" +
                "friendId=" + friendId +
                ", user=" + user +
                '}';
    }
}
