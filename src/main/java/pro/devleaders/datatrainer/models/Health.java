package pro.devleaders.datatrainer.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Builder
//@NoArgsConstructor
@AllArgsConstructor
@Table(name = "health")
@EqualsAndHashCode(of = {"creator"})
public class Health extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    private String bad;

    @Size(max = 50)
    private String chair;

    @Size(max = 8)
    private String urine;

    private float weight;

    @Size(max = 50)
    private String sleep;

    @Size(max = 50)
    private String sam;

    @Size(max = 50)
    private String pes;

    @Size(max = 50)
    private String muscles;

    @Size(max = 200)
    private String mchk;

    @Size(max = 50)
    private String sport;

    @Size(max = 50)
    private String training;

    private int rpe;

    @Size(max = 50)
    private String la;

    @Size(max = 50)
    private String breath;

    @Size(max = 50)
    private String sweat;

    private float degrees;

    @Temporal(TemporalType.DATE)
    private Date date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "creator", nullable = false)
    private User creator;

    public Health( ) { };

    public Health(
        String bad,
        String chair,
        String urine,
        float weight,
        String sleep,
        String sam,
        String pes,
        String muscles,
        String mchk,
        String sport,
        String training,
        int rpe,
        String la,
        String breath,
        String sweat,
        float degrees,
         Date date
    ) {
        this.bad = bad;
        this.chair = chair;
        this.urine = urine;
        this.weight = weight;
        this.sleep = sleep;
        this.sam = sam;
        this.pes = pes;
        this.muscles = muscles;
        this.mchk = mchk;
        this.sport = sport;
        this.training = training;
        this.rpe = rpe;
        this.la =  la;
        this.breath = breath;
        this.sweat = sweat;
        this.degrees = degrees;
        this.date = date;
    }

    public Long getId() {return id;}
    public void setId(Long id) { this.id = id; }

    public String getBad() { return bad; }
    public void setBad(String bad) { this.bad = bad; }

    public String getChair() { return chair; }
    public void setChair(String chair) { this.chair = chair; }

    public String getUrine() { return urine; }
    public void setUrine(String urine) { this.urine = urine; }

    public float getWeight() { return weight; }
    public void setWeight(float weight) { this.weight = weight; }

    public String getSleep() { return sleep; }
    public void setSleep(String sleep) { this.sleep = sleep; }

    public String getSam() { return sam; }
    public void setSam(String sam) { this.sam = sam; }

    public String getPes() { return pes; }
    public void setPes(String pes) { this.pes = pes; }

    public String getMuscles() { return muscles; }
    public void setMuscles(String muscles) { this.muscles = muscles; }

    public String getMchk() { return mchk; }
    public void setMchk(String mchk) { this.mchk = mchk; }

    public String getSport() { return sport; }
    public void setSport(String sport) { this.sport = sport; }

    public String getTraining() { return training; }
    public void setTraining(String training) { this.training = training; }

    public int  getRpe() { return rpe; }
    public void setRpe(int rpe) { this.rpe = rpe; }

    public String getLa() { return la; }
    public void setLa(String la) { this.la = la; }

    public String getBreath() { return breath; }
    public void setBreath(String breath) { this.breath = breath; }

    public String getSweat() { return sweat; }
    public void setSweat(String sweat) { this.sweat = sweat; }

    public float getDegrees() { return degrees; }
    public void setDegrees(float degrees) { this.degrees = degrees; }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public User getCreator() {
        return creator;
    }
    public void setCreator(User creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        return "Health{" +
                "id=" + id +
                ", bad='" + bad + '\'' +
                ", chair='" + chair + '\'' +
                ", urine='" + urine + '\'' +
                ", weight=" + weight +
                ", sleep='" + sleep + '\'' +
                ", sam='" + sam + '\'' +
                ", pes='" + pes + '\'' +
                ", muscles='" + muscles + '\'' +
                ", mchk='" + mchk + '\'' +
                ", sport='" + sport + '\'' +
                ", training='" + training + '\'' +
                ", rpe=" + rpe +
                ", la='" + la + '\'' +
                ", breath='" + breath + '\'' +
                ", sweat='" + sweat + '\'' +
                ", degrees=" + degrees +
                ", date=" + date +
                ", creator=" + creator +
                '}';
    }
}
