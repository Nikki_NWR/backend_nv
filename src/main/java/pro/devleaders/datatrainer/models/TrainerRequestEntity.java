package pro.devleaders.datatrainer.models;

import java.util.List;

public class TrainerRequestEntity {

    private List<String> trainer;

    public List<String> getTrainer() {
        return trainer;
    }

    public void setTrainer(List<String> trainer) {
        this.trainer = trainer;
    }
}
