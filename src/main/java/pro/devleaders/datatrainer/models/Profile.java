package pro.devleaders.datatrainer.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(	name = "profiles" )
public class Profile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Size(max = 50)
	private String fname;

	@Size(max = 50)
	private String sname;

	private Long age;

	private Long height;

	private Long weight;

	private String gender;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "profile_train_sports",
				joinColumns = @JoinColumn(name = "user_id"),
				inverseJoinColumns = @JoinColumn(name = "sport_id"))
	private Set<Sport> trainSports = new HashSet<>();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "profile_go_sports",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "sport_id"))
	private Set<Sport> goSports = new HashSet<>();


	@OneToOne(fetch = FetchType.LAZY,
			cascade =  CascadeType.ALL,
			mappedBy = "profile")
	@JsonManagedReference
	@JsonIgnore
	@ApiModelProperty(notes = "Variable linking the user with his user profile")
	private User user;

	public Profile() {
	}

	public Profile(String fname, String sname, Long age, Long height, Long weight, String gender) {
		this.fname = fname;
		this.sname = sname;
		this.age = age;
		this.height = height;
		this.weight = weight;
		this.gender = gender;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getFName() {
		return fname;
	}
	public void setFName(String fname) {
		this.fname = fname;
	}

	public String getSName() {
		return sname;
	}
	public void setSName(String sname) {
		this.sname = sname;
	}

	public Long getAge() {
		return age;
	}
	public void setAge(Long age) {
		this.age = age;
	}

	public Long getHeight() {
		return height;
	}
	public void setHeight(Long height) {
		this.height = height;
	}

	public Long getWeight() {
		return weight;
	}
	public void setWeight(Long weight) {
		this.weight = weight;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public Set<Sport> getTrainSports() {
		return trainSports;
	}
	public void setTrainSports(Set<Sport> trainSports) {
		this.trainSports = trainSports;
	}

	public Set<Sport> getGoSports() {
		return goSports;
	}
	public void setGoSports(Set<Sport> goSports) {
		this.goSports = goSports;
	}

	@Override
	public String toString() {
		return "Profile{" +
				"id=" + id +
				", fname='" + fname + '\'' +
				", sname='" + sname + '\'' +
				", age=" + age +
				", height=" + height +
				", weight=" + weight +
				", gender='" + gender + '\'' +
				", trainSports=" + trainSports +
				", goSports=" + goSports +
				", user=" + user +
				'}';
	}
}
