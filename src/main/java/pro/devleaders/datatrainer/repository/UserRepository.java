package pro.devleaders.datatrainer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pro.devleaders.datatrainer.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User getById(Long userId);

	User getByEmail(String email);

	User findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
}
