package pro.devleaders.datatrainer.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pro.devleaders.datatrainer.models.Health;
import pro.devleaders.datatrainer.models.User;

import java.util.List;

@Repository
public interface HealthRepository extends JpaRepository<Health, Long > {
//    Health getById(Long profileId);


    @Query("SELECT m " +
            "FROM Health m " +
            "WHERE m.creator = :creator"
//            "ORDER BY m.posted DESC"
    )
    List<Health> findByRecipientOrSenderOrderByPostedDesc(
            @Param("creator") User creator
    );

}
