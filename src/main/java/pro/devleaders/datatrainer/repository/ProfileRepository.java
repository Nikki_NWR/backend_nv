package pro.devleaders.datatrainer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.devleaders.datatrainer.models.Profile;

@Repository
public interface ProfileRepository extends JpaRepository< Profile, Long > {
    Profile getById(Long profileId);
}
