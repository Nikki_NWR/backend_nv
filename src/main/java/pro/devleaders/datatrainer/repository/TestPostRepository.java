package pro.devleaders.datatrainer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pro.devleaders.datatrainer.models.TestPost;


public interface TestPostRepository extends JpaRepository <TestPost, Long> {
    TestPost getById(Long testPostId);

//    List<TestPost> findAllByOrderByIdDesc();
//   TestPost findByName(String text,String prop_1,String prop_2,String prop_3);

}

