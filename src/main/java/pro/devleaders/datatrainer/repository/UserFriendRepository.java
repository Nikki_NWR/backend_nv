package pro.devleaders.datatrainer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.devleaders.datatrainer.models.User;

import java.util.List;

@Repository
public interface UserFriendRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
    List<User> findAllByEmail(String email);
}
