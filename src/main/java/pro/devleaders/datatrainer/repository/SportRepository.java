package pro.devleaders.datatrainer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.devleaders.datatrainer.models.ESport;
import pro.devleaders.datatrainer.models.Sport;

import java.util.Optional;

@Repository
public interface SportRepository extends JpaRepository<Sport, Long> {
	Optional<Sport> findByName(ESport name);
}
