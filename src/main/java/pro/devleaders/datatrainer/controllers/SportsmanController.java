package pro.devleaders.datatrainer.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pro.devleaders.datatrainer.models.SportsmanRequestEntity;
import pro.devleaders.datatrainer.services.SportsmanService;

import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController(value = "/Sport")
@RequestMapping("/api/Sport")
@Api( tags = "Профиль спортсмена")
public class SportsmanController{

    @Autowired
    private SportsmanService sportsmanService;

    @PostMapping(value = "/userSportsmanRequest")
    @ApiOperation(value = "добавление  спортсмена")
    public ResponseEntity<Map<String, Object>> userSportsmanRequest(@RequestBody SportsmanRequestEntity sportsmanRequestEntity) {
        return this.sportsmanService.addUserSportsman(sportsmanRequestEntity);
    }
}
