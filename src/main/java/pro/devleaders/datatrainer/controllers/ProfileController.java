package pro.devleaders.datatrainer.controllers;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pro.devleaders.datatrainer.RestResponse;
import pro.devleaders.datatrainer.exception.ResourceNotFoundException;
import pro.devleaders.datatrainer.models.Profile;
import pro.devleaders.datatrainer.repository.ProfileRepository;
import pro.devleaders.datatrainer.services.ProfileService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController(value = "/profile")
@RequestMapping("/api/profile")
@Api( tags = "Карточки пользователей")
public class ProfileController {

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    ProfileService profileService;

    //Getters
    @GetMapping("/getAll")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Получить список всех профилей")
    List<Profile> getAll(){
        return profileRepository.findAll();
    }

    @GetMapping("/getById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Получить профиль по id")
    public Profile getById(@PathVariable("id") Long id) {
        return profileRepository.getOne(id);
    }

    //Actions
    @PostMapping("/add")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Создать профиль")
    public Profile add(@RequestBody Profile newProfile){
        return profileRepository.save(newProfile);
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Удалить профиль")
    void delete(@PathVariable Long id){
        profileRepository.deleteById(id);
    }

    //Mutations
    @PutMapping("/edit/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Редактировать профиль")
    public RestResponse<?> updateProfile(@PathVariable Long id, @Valid @RequestBody Profile profileRequest) {
        try {
            return RestResponse.createSuccessResponse(profileService.EditProfile(id, profileRequest));
        } catch (ResourceNotFoundException e) {
            return RestResponse.createFailureResponse(e.getMessage(), 400);
        }
    }

//    @ApiOperation(value = "Persons search")
//    @GetMapping("/search")
//    public Page<ProfileView> getPeople(
//            @RequestParam(name = "searchTerm", defaultValue = "", required = false) String searchTerm,
//            @PageableDefault(size = 20) Pageable pageRequest) {
//        log.debug("REST request to get people list (searchTerm:{}, pageRequest:{})", searchTerm, pageRequest);
//
//        final Page<Person> people = personService.getPeople(searchTerm, pageRequest);
//
//        return people.map(PersonView::new);
//    }




}
