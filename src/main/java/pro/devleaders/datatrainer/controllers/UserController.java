package pro.devleaders.datatrainer.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pro.devleaders.datatrainer.models.*;
import pro.devleaders.datatrainer.repository.UserRepository;
import pro.devleaders.datatrainer.security.SecurityUtils;
import pro.devleaders.datatrainer.services.UserDetailsServiceImpl;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/users")
@RestController(value = "/users")
@Api( tags = "Учетные записи")
public class UserController {
	@Autowired
	UserRepository userRepository;

	@Autowired
	UserDetailsServiceImpl userService;


	@GetMapping("/getById/{id}")
	@ApiOperation(value = "Получить информацию по айди")
	public ResponseEntity<UserView> getPerson( @PathVariable("id") long id ) {
		final User user = userService.findById(id);
		if (null == user) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(new UserView(user));
	}



	@GetMapping("/getByEmail/{username}")
	@ApiOperation(value = "Получить mail")
	public User getByUsername(@PathVariable("username") String username) {
		return userRepository.findByUsername(username);
	}

	@GetMapping("/getAllUsers")
	@ApiOperation(value = "Получить всех пользователей")
	List<User> all(){ return userRepository.findAll();}


	@GetMapping("/getAllUsersView")
	@ApiOperation(value = "Получить всех пользователей  UserView")
	public ResponseEntity<UserView> allUser(User user){
		List<User> allView = userRepository.findAll();
		return ResponseEntity.ok(new UserView(user));
	}


	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	@GetMapping("/getMe")
	@ApiOperation(value = "Получить информацию по мне")
	public User getMe() {
		String username = SecurityUtils.currentLogin();
		User user = userRepository.findByUsername(username);
		System.out.println(user);
		return user;
	}


	@GetMapping("/getMeAll")
	@ApiOperation(value = "Получить информацию по мне")
	public ResponseEntity<UserView> getMeAll() {
		String username = SecurityUtils.currentLogin();
		User user = userRepository.findByUsername(username);
		System.out.println(user);
		return ResponseEntity.ok(new UserView(user));
	}
}
