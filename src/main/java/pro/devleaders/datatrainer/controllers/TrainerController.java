package pro.devleaders.datatrainer.controllers;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pro.devleaders.datatrainer.models.TrainerRequestEntity;
import pro.devleaders.datatrainer.services.TrainerService;


import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController(value = "/Trainer")
@RequestMapping("/api/Trainer")
@Api( tags = "Профиль тренера")
public class TrainerController {

    @Autowired
    private TrainerService trainerService;

    @PostMapping(value = "/userTrainerRequest")
    @ApiOperation(value = "добавление  тренера")
    public ResponseEntity<Map<String, Object>> userTrainerRequest(@RequestBody TrainerRequestEntity trainerRequestEntity) {
        return this.trainerService.addUserTrainer(trainerRequestEntity);
    }
}
