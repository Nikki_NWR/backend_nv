package pro.devleaders.datatrainer.controllers;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pro.devleaders.datatrainer.RestResponse;
import pro.devleaders.datatrainer.exception.ResourceNotFoundException;
import pro.devleaders.datatrainer.models.TestPost;
import pro.devleaders.datatrainer.repository.ProfileRepository;
import pro.devleaders.datatrainer.repository.TestPostRepository;
import pro.devleaders.datatrainer.services.TestPostService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
//@RestController(value = "/TestPost")
//@RequestMapping("/api/TestPost")
@Api( tags = "TestPost controller")

public class TestPostController {

    @Autowired
    private final TestPostRepository testPostRepository;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    TestPostService testPostService;

    TestPostController(TestPostRepository testPostRepository) {
        this.testPostRepository = testPostRepository;
    }

    @PostMapping("/create")
    TestPost newTestPost(@RequestBody TestPost newTestPost) {
        return testPostRepository.save(newTestPost);
    }

    @GetMapping("/getAll")
    List<TestPost> all(){ return testPostRepository.findAll();}

    @DeleteMapping("/deleteById/{testPostId}")
    @ApiOperation(value = "Delete a post based on the testPostId")
    public RestResponse<?>  deleteTestPost(@PathVariable Long testPostId) {


        try{return RestResponse.createSuccessResponse(testPostService.deleteTestPost(testPostId)); }
        catch(ResourceNotFoundException e){
            return RestResponse.createFailureResponse(e.getMessage(),400);
        }
    }

    @PutMapping("/editPostById/{postId}")
    @ApiOperation(value = "Update a post based on the post id")
    public RestResponse<?> updateTestPost(@PathVariable Long postId, @Valid @RequestBody TestPost testPostRequest) {
        try {
            return RestResponse.createSuccessResponse(testPostService.EditTestPost(postId, testPostRequest));

        } catch (ResourceNotFoundException e) {
            return RestResponse.createFailureResponse(e.getMessage(), 400);
        }
    }

    @GetMapping("/getById/{TestPostId:\\d+}")
    public TestPost getById(@PathVariable("TestPostId") long id) {
        return testPostRepository.getById(id);
    }

//    @GetMapping("/TestPost/{TestPostName:\\D+}")
//    public TestPost getTestPostByName(@PathVariable("TestPostName") String text) {
//        return countryService.getByName(name);
//    }
}

