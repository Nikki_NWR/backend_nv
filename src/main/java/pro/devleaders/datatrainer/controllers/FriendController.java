package pro.devleaders.datatrainer.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pro.devleaders.datatrainer.models.BlockUserRequestEntity;
import pro.devleaders.datatrainer.models.SubscribeUserRequestEntity;
import pro.devleaders.datatrainer.models.UserFriendsRequestEntity;
import pro.devleaders.datatrainer.services.BlockUserService;
import pro.devleaders.datatrainer.services.SubscribeUserService;
import pro.devleaders.datatrainer.services.UserFriendsService;

import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController(value = "/Friend")
@RequestMapping("/api/Friend")
@Api( tags = "Добавление в друзья")
public class FriendController {

    @Autowired
    private UserFriendsService userFriendsService;

    @Autowired
    private SubscribeUserService subscribeUserService;

    @Autowired
    private BlockUserService blockUserService;

    @PostMapping(value = "/userFriendRequest")
    @ApiOperation(value = "Запрос на добавление в друзья ")
    public ResponseEntity<Map<String, Object>> userFriendRequest(@RequestBody UserFriendsRequestEntity userFriendsRequestEntity) {
        return this.userFriendsService.addUserFriends(userFriendsRequestEntity);
    }

    @PostMapping(value = "/getCommonUserFriends")
    @ApiOperation(value = "Получить список общих друзей")
    public ResponseEntity<Map<String, Object>> getCommonUserFriends(@RequestBody UserFriendsRequestEntity userFriendsRequestEntity) {
        return this.userFriendsService.getCommonUserFriends(userFriendsRequestEntity);
    }

    @PostMapping(value = "/subscribeUserRequest")
    @ApiOperation(value = "Запрос на подписку")
    public ResponseEntity<Map<String, Object>> subscribeUserRequest(@RequestBody SubscribeUserRequestEntity subscribeUserRequestEntity) {
        return this.subscribeUserService.addSubscribeUser(subscribeUserRequestEntity);
    }

    @PostMapping(value = "/blockUserRequest")
    @ApiOperation(value = "Запрос на добавление в блок")
    public ResponseEntity<Map<String, Object>> blockUserRequest(@RequestBody BlockUserRequestEntity blockUserRequestEntity) {
        return this.blockUserService.addBlockUser(blockUserRequestEntity);
    }
}
