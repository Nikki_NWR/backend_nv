package pro.devleaders.datatrainer.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pro.devleaders.datatrainer.RestResponse;
import pro.devleaders.datatrainer.exception.ResourceNotFoundException;
import pro.devleaders.datatrainer.models.*;
import pro.devleaders.datatrainer.repository.UserRepository;
import pro.devleaders.datatrainer.security.SecurityUtils;
import pro.devleaders.datatrainer.services.HealthServiceImpl;
import pro.devleaders.datatrainer.services.UserDetailsServiceImpl;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController(value = "/health")
@RequestMapping("/api/health")
@Api( tags = "Дневник Здоровья")
public class HealthController {

    @Autowired
    HealthServiceImpl healthService;

    @Autowired
    UserDetailsServiceImpl userService;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/getById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Получить запись о здоровье по id")
    public ResponseEntity<HealthView> getPerson( @PathVariable("id") Long id ) {
        final Health health = healthService.getHealthById(id);
        if (null == health) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(new HealthView(health));
    }

    @GetMapping(value = "/getHealthByMe/")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Получить записи о здоровье авторизованного пользователя")
    public ResponseEntity<List<HealthView>> getHealthByMe() {
        String username = SecurityUtils.currentLogin();
        User user = userRepository.findByUsername(username);
        return ResponseEntity.ok(map(healthService.getDialog(user)));
    }

    @GetMapping(value = "/getHealthByUser/{userId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Получить записи о здоровье по UserId")
    public ResponseEntity<List<HealthView>> getHealthByUser(@PathVariable Long userId) {
        User user = userRepository.getById(userId);
        return ResponseEntity.ok(map(healthService.getDialog(user)));
    }

    @PostMapping("/add")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Добавить запись о здоровье")
    public Health add(@RequestBody @Valid HealthPost healthPost) {
        final Health health = new Health();
        String username = SecurityUtils.currentLogin();
        User user = userRepository.findByUsername(username);
        health.setCreator(userService.findById(user.getId()));
        healthService.add(health);
        return health;
    }


    @PutMapping("/edit/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @ApiOperation(value = "Редактировать запись и здоровье")
    public RestResponse<?> updateHealth(@PathVariable Long HealthId, @Valid @RequestBody Health healthRequest) {
        try {
            return RestResponse.createSuccessResponse(healthService.EditHealth(HealthId, healthRequest));
        } catch (ResourceNotFoundException e) {
            return RestResponse.createFailureResponse(e.getMessage(), 400);
        }
    }

    private List<HealthView> map(List<Health> health) {
        return health.stream()
                .map(HealthView::new)
                .collect(toList());
    }

}
