package pro.devleaders.datatrainer.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.models.Profile;

@Service
public interface ProfileService {

    Profile EditProfile(Long Id, Profile profile);

    ResponseEntity<?> deleteById (Long Id);
}
