package pro.devleaders.datatrainer.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.models.SportsmanRequestEntity;

import java.util.Map;

@Service
public interface SportsmanService {

    ResponseEntity<Map<String, Object>> addUserSportsman(SportsmanRequestEntity sportsmanRequestEntity);
}
