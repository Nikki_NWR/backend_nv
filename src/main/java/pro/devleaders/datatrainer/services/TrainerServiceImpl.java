package pro.devleaders.datatrainer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import pro.devleaders.datatrainer.models.TrainerRequestEntity;
import pro.devleaders.datatrainer.models.User;
import pro.devleaders.datatrainer.repository.TrainerRepository;


import java.util.HashMap;
import java.util.Map;

@Service
public class TrainerServiceImpl implements TrainerService {

    @Autowired
    private TrainerRepository trainerRepository;


    private User saveIfNotExist(String email) {

        User existingUser = this.trainerRepository.findByEmail(email);
        if (existingUser == null) {
            existingUser = new User();
            existingUser.setEmail(email);
            return this.trainerRepository.save(existingUser);
        } else {
            return existingUser;
        }

    }

    @Override
    public ResponseEntity<Map<String, Object>> addUserTrainer(TrainerRequestEntity trainerRequestEntity) {

        Map<String, Object> result = new HashMap<String, Object>();

        if (trainerRequestEntity == null) {
            result.put("Error : ", "Invalid request Trainer");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        if (CollectionUtils.isEmpty(trainerRequestEntity.getTrainer())) {
            result.put("Error : ", "Trainer list cannot be empty");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }
        if (trainerRequestEntity.getTrainer().size() != 2) {
            result.put("Info : ", "Please provide 2 emails to make them friends");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        String email1 = trainerRequestEntity.getTrainer().get(0);
        String email2 = trainerRequestEntity.getTrainer().get(1);

        if (email1.equals(email2)) {
            result.put("Info : ", "Cannot make Trainer, if users are same");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        User user1 = null;
        User user2 = null;
        user1 = this.saveIfNotExist(email1);
        user2 = this.saveIfNotExist(email2);

        if (user1.getTrainer().contains(user2)) {
            result.put("Info : ", "Can't add, they are already Trainer");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
        }

        if (user1.getBlockUsers().contains(user2)) {
            result.put("Info : ", "Can't add, Trainer are blocked ");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
        }

        user1.addUserTrainer(user2);
        this.trainerRepository.save(user1);
        result.put("Success", true);

        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }

}
