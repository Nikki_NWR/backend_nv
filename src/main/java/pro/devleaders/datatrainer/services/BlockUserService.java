package pro.devleaders.datatrainer.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.models.BlockUserRequestEntity;

import java.util.Map;

@Service
public interface BlockUserService {

    ResponseEntity<Map<String, Object>> addBlockUser(BlockUserRequestEntity BlockUserRequestEntity);
}
