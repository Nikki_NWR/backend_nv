package pro.devleaders.datatrainer.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.models.TrainerRequestEntity;

import java.util.Map;

@Service
public interface TrainerService {

    ResponseEntity<Map<String, Object>> addUserTrainer(TrainerRequestEntity trainerRequestEntity);
}
