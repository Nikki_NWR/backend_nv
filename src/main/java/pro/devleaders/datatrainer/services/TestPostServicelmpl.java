package pro.devleaders.datatrainer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.exception.ResourceNotFoundException;
import pro.devleaders.datatrainer.models.Profile;
import pro.devleaders.datatrainer.models.TestPost;
import pro.devleaders.datatrainer.repository.ProfileRepository;
import pro.devleaders.datatrainer.repository.TestPostRepository;

import java.util.Optional;

@Service
public class TestPostServicelmpl implements TestPostService {

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    TestPostRepository testpostRepository;


    @Override
    public TestPost InsertTestPost(TestPost testPost, long ProfileId) {

        Optional<Profile> profile =profileRepository.findById(ProfileId);
        if(profile != null) {
            testPost.setProfile(profile.get());
            return testPost;
        }
        else{throw new ResourceNotFoundException("User Profile Is Not Found");
        }
    }

    @Override
    public TestPost EditTestPost(Long testPostId, TestPost testPost) {

        return testpostRepository.findById(testPostId).map(UpdatedtestPost -> {
            UpdatedtestPost.setText(testPost.getText());
            UpdatedtestPost.setProp_1(testPost.getProp_1());
            UpdatedtestPost.setProp_2(testPost.getProp_2());
            UpdatedtestPost.setProp_3(testPost.getProp_3());
            return testpostRepository.save(UpdatedtestPost);
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + testPostId + " not found"));

    }

    @Override
    public ResponseEntity<?> deleteTestPost(Long testPostId) {
        return testpostRepository.findById(testPostId).map(testPost -> {
            testpostRepository.delete(testPost);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + testPostId + " not found"));
    }


    public TestPost getTestPostById(Long id) {
        TestPost testPost;
        testPost = testpostRepository.getOne(id);
        if (testPost == null) {
            throw new ResourceNotFoundException("TestPost c id не найден" + id);
        }
        return testPost;
    }

}
