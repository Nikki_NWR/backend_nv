package pro.devleaders.datatrainer.services;

import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.models.Health;

@Service
public interface HealthService {

    Health EditHealth(Long Id, Health health);

}
