package pro.devleaders.datatrainer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.exception.ResourceNotFoundException;
import pro.devleaders.datatrainer.models.Profile;
import pro.devleaders.datatrainer.repository.ProfileRepository;

@Service
public class ProfileServiceImpl implements ProfileService{

    @Autowired
    ProfileRepository profileRepository;

    @Override
    public ResponseEntity<?> deleteById (Long id) {
        return profileRepository.findById(id).map(profile -> {
            profileRepository.delete(profile);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + id + " not found"));
    }

    public Profile getProfileById(Long id) {
        Profile profile;
        profile = profileRepository.getOne(id);
        if (profile == null) {
            throw new ResourceNotFoundException("Profile c id не найден" + id);
        }
        return profile;
    }

    public Profile EditProfile(Long Id, Profile profile) {

        return profileRepository.findById(Id).map(UpdatedProfile -> {
            UpdatedProfile.setFName(profile.getFName());
            UpdatedProfile.setSName(profile.getSName());
            return profileRepository.save(UpdatedProfile);
        }).orElseThrow(() -> new ResourceNotFoundException("Id " + Id + " not found"));

    }
}
