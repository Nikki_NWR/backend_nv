package pro.devleaders.datatrainer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import pro.devleaders.datatrainer.models.User;
import pro.devleaders.datatrainer.models.UserFriendsRequestEntity;
import pro.devleaders.datatrainer.repository.UserFriendRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserFriendsServiceImpl implements UserFriendsService {

    @Autowired
    private UserFriendRepository userFriendRepository;

    private User saveIfNotExist(String email) {

        User existingUser = this.userFriendRepository.findByEmail(email);
        if (existingUser == null) {
            existingUser = new User();
            existingUser.setEmail(email);
            return this.userFriendRepository.save(existingUser);
        } else {
            return existingUser;
        }

    }

    @Override
    public ResponseEntity<Map<String, Object>> addUserFriends(UserFriendsRequestEntity userFriendsRequestEntity) {

        Map<String, Object> result = new HashMap<String, Object>();

        if (userFriendsRequestEntity == null) {
            result.put("Error : ", "Invalid request");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        if (CollectionUtils.isEmpty(userFriendsRequestEntity.getFriends())) {
            result.put("Error : ", "Friend list cannot be empty");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }
        if (userFriendsRequestEntity.getFriends().size() != 2) {
            result.put("Info : ", "Please provide 2 emails to make them friends");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        String email1 = userFriendsRequestEntity.getFriends().get(0);
        String email2 = userFriendsRequestEntity.getFriends().get(1);

        if (email1.equals(email2)) {
            result.put("Info : ", "Cannot make friends, if users are same");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        User user1 = null;
        User user2 = null;
        user1 = this.saveIfNotExist(email1);
        user2 = this.saveIfNotExist(email2);

        if (user1.getUserFriends().contains(user2)) {
            result.put("Info : ", "Can't add, they are already friends");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
        }

        if (user1.getBlockUsers().contains(user2)) {
            result.put("Info : ", "Can't add, friends are blocked ");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
        }

        user1.addUserFriends(user2);
        this.userFriendRepository.save(user1);
        result.put("Success", true);

        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<Map<String, Object>> getCommonUserFriends(UserFriendsRequestEntity userFriendsRequestEntity) {

        Map<String, Object> result = new HashMap<String, Object>();

        if (userFriendsRequestEntity == null) {
            result.put("Error : ", "Invalid request");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        User user1 = null;
        User user2 = null;
        user1 = this.userFriendRepository.findByEmail(userFriendsRequestEntity.getFriends().get(0));
        user2 = this.userFriendRepository.findByEmail(userFriendsRequestEntity.getFriends().get(1));

        if (user1.getEmail().equals(user2.getEmail())) {
            result.put("Info : ", "Both users are same");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        Set<User> friends = null;
        friends = user1.getUserFriends();
        friends.retainAll(user2.getUserFriends());

        result.put("success", true);
        result.put("friends", friends.stream().map(User::getEmail).collect(Collectors.toList()));
        result.put("count", friends.size());

        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }
}
