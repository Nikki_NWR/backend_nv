package pro.devleaders.datatrainer.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.exception.ResourceNotFoundException;
import pro.devleaders.datatrainer.models.Health;
import pro.devleaders.datatrainer.models.User;
import pro.devleaders.datatrainer.repository.HealthRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HealthServiceImpl implements HealthService {

    Health health;

    @Autowired
    HealthRepository healthRepository;

    @Override
    public Health EditHealth(Long Id, Health health) {
        return healthRepository.findById(Id).map(UpdatedHealth -> {
            UpdatedHealth.setBad(health.getBad());
            UpdatedHealth.setChair(health.getChair());
            UpdatedHealth.setUrine(health.getUrine());
            UpdatedHealth.setWeight(health.getWeight());
            UpdatedHealth.setSleep(health.getSleep());
            UpdatedHealth.setSam(health.getSam());
            UpdatedHealth.setPes(health.getPes());
            UpdatedHealth.setMuscles(health.getMuscles());
            UpdatedHealth.setMchk(health.getMchk());
            UpdatedHealth.setSport(health.getSport());
            UpdatedHealth.setTraining(health.getTraining());
            UpdatedHealth.setRpe(health.getRpe());
            UpdatedHealth.setLa(health.getLa());
            UpdatedHealth.setBreath(health.getBreath());
            UpdatedHealth.setSweat(health.getSweat());
            UpdatedHealth.setDegrees(health.getDegrees());
            UpdatedHealth.setDate(health.getDate());
            return healthRepository.save(UpdatedHealth);
        }).orElseThrow(() -> new ResourceNotFoundException("Id " + Id + " not found"));

    }

    public Health getHealthById(Long id) {
        health = healthRepository.getOne(id);
        if (health == null) {
            throw new ResourceNotFoundException("TestPost c id не найден" + id);
        }
        return health;
    }

    public Health add(Health health) {
        return healthRepository.save(health);
    }

    public List<Health> getDialog(User user) {
        return healthRepository.findByRecipientOrSenderOrderByPostedDesc(user);
    }


}
