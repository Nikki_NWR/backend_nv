package pro.devleaders.datatrainer.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pro.devleaders.datatrainer.models.TestPost;

@Service
public interface TestPostService {

    TestPost InsertTestPost(TestPost testPost,long ProfileId);

    TestPost EditTestPost(Long testPostId,TestPost testPost);

    ResponseEntity<?> deleteTestPost(Long testPostId);

}
