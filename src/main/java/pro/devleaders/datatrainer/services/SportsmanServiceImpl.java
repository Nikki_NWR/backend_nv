package pro.devleaders.datatrainer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import pro.devleaders.datatrainer.models.SportsmanRequestEntity;
import pro.devleaders.datatrainer.models.User;
import pro.devleaders.datatrainer.repository.SportsmanRepository;

import java.util.HashMap;
import java.util.Map;

@Service
public class SportsmanServiceImpl implements SportsmanService {

    @Autowired
    private SportsmanRepository sportsmanRepository;

    private User saveIfNotExist(String email) {

        User existingUser = this.sportsmanRepository.findByEmail(email);
        if (existingUser == null) {
            existingUser = new User();
            existingUser.setEmail(email);
            return this.sportsmanRepository.save(existingUser);
        } else {
            return existingUser;
        }

    }

    @Override
    public ResponseEntity<Map<String, Object>> addUserSportsman(SportsmanRequestEntity sportsmanRequestEntity) {

        Map<String, Object> result = new HashMap<String, Object>();

        if (sportsmanRequestEntity == null) {
            result.put("Error : ", "Invalid request Sportsman");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        if (CollectionUtils.isEmpty(sportsmanRequestEntity.getSportsman())) {
            result.put("Error : ", "Sportsman list cannot be empty");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }
        if (sportsmanRequestEntity.getSportsman().size() != 2) {
            result.put("Info : ", "Please provide 2 emails to make them Sportsman");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        String email1 = sportsmanRequestEntity.getSportsman().get(0);
        String email2 = sportsmanRequestEntity.getSportsman().get(1);

        if (email1.equals(email2)) {
            result.put("Info : ", "Cannot make Sportsman, if users are same");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
        }

        User user1 = null;
        User user2 = null;
        user1 = this.saveIfNotExist(email1);
        user2 = this.saveIfNotExist(email2);

        if (user1.getSportsman().contains(user2)) {
            result.put("Info : ", "Can't add, they are already Trainer");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
        }

        if (user1.getBlockUsers().contains(user2)) {
            result.put("Info : ", "Can't add, Trainer are blocked ");
            return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
        }

        user1.addUserSportsman(user2);
        this.sportsmanRepository.save(user1);
        result.put("Success", true);

        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }
}
